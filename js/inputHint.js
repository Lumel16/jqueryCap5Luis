$(document).ready(function(){
	/*Ocultar todos los div de cada ejercicio*/
	$('.Ejercicios').hide();
	$('.module').hide();

	/*Mostrar ejercicio 1*/
	$("#ex1-1").on('click', function(event) {
		$(".Ejercicio1-1").toggle();
	});

	/*Ejecuta el ejercicio 1*/
	$('#text').on('keypress', function(event) {
		if(event.keyCode == 13)
	    {
	        $('label[for=text]').html($(this).val());
	    }
	});

	/*Mostrar ejercicio 1.2*/
	$("#ex1-2").on('click', function(event) {
		$(".Ejercicio1-2").toggle();
	});

	/*Ejecuta el ejercicio 1.2*/
	$('#ej1-2').on('click', function(event) {
		$('#hint').addClass('hint');
	});

	/*Mostrar ejercicio 1.3*/
	$("#ex1-3").on('click', function(event) {
		$(".Ejercicio1-3").toggle();
	});

	/*Ejecuta el ejercicio 1.3*/
	$('#ej1-3').on('click', function(event) {
		$('label[for=lblRemove]').remove();
	});

	/*Mostrar ejercicio 1.4*/
	$("#ex1-4").on('click', function(event) {
		$(".Ejercicio1-4").toggle();
	});

	/*Ejecuta el ejercicio 1.4*/
	var $input = $('#txtFocus');
	var value;
	$('#txtFocus').on({
		'focusin': function(){
			value = $input.val();
			$input.val('');
		},
		'focusout': function(){
			$(this).removeClass('hint');
			if(!$input.val())
				$input.val(value);
			else
				value = $input.val();
		}
	});

	/*Mostrar ejercicio 2.0*/
	$("#ex2-0").on('click', function(event) {
		$(".Ejercicio2-0").toggle();
	});

	/*Ejecuta el ejercicio 2.0*/
	var nav1 = $('#nav1');
	var nav2 = $('#nav2');
	var nav3 = $('#nav3');
	nav1.addClass('current');
	$('#opcion1').toggle();
	$(nav1).on('click', function(event) {
		var condicion = $(this).hasClass('current');
		if (!condicion) {
			$(this).addClass('current');
			$(this).siblings().each(function(index, el) {
				$(el).removeClass('current');
			});
			var div = $('#bodytabs #opcion1');
			div.toggle(500);
			div.siblings().each(function(index, el) {
				if($(el).is(':visible')) $(el).toggle(500);
			});
		};
	});

	$(nav2).on('click', function(event) {
		var condicion = $(this).hasClass('current');
		if (!condicion) {
			$(this).addClass('current');
			$(this).siblings().each(function(index, el) {
				$(el).removeClass('current');
			});
			var div = $('#bodytabs #opcion2');
			div.toggle(500);
			div.siblings().each(function(index, el) {
				if($(el).is(':visible')) $(el).toggle(500);
			});
		};
	});

	$(nav3).on('click', function(event) {
		var condicion = $(this).hasClass('current');
		if (!condicion) {
			$(this).addClass('current');
			$(this).siblings().each(function(index, el) {
				$(el).removeClass('current');
			});
			var div = $('#bodytabs #opcion3');
			div.toggle(500);
			div.siblings().each(function(index, el) {
				if($(el).is(':visible')) $(el).toggle(500);
			});
		};
	});



	/******/
	});